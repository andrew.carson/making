
poly_n = 300;

module presch_screwdriver (
  shank_diameter,
  shank_length,
  handle_first_diameter,
  handle_second_diameter,
  handle_third_diameter,
  handle_first_length,
  handle_second_length ) {

  handle_middle_length=14;

  epsilon=0.01;
 
  r_increase=0.5;
  l_increase=0.75;
 
  color("grey") cylinder(
    h=shank_length+epsilon,
    r=r_increase + shank_diameter/2,
    $fn=poly_n);
    
  translate([0,0,shank_length]) {
    color("red") cylinder(
      h=l_increase+handle_first_length-(handle_middle_length/2)+epsilon,
      r1=r_increase+handle_first_diameter/2,
      r2=r_increase+handle_second_diameter/2,
      $fn=poly_n);
  }
  translate([0,0,shank_length+l_increase+handle_first_length-(handle_middle_length/2)]) {
    color("blue") cylinder(
      h=handle_middle_length,
      r1=r_increase+handle_second_diameter/2,
      r2=r_increase+handle_second_diameter/2,
      $fn=poly_n);
  }
  translate([0,0,shank_length+l_increase+handle_first_length+(handle_middle_length/2)-epsilon]) {
    color("orange") cylinder(
      h=l_increase+handle_second_length-(handle_middle_length/2)+epsilon,
      r1=r_increase+handle_second_diameter/2,
      r2=r_increase+handle_third_diameter/2,
      $fn=poly_n);
  }
}

function driver_length(driver) = driver[1] + driver[5] + driver[6];

function z_translation(driver_length, block_length) = -(driver_length - block_length)/2;

translate([0,0,0]) {
  block_length = 210;
  block_width = 168;
  
  //  shank_diameter
  //  shank_length
  //  handle_first_diameter
  //  handle_second_diameter
  //  handle_third_diameter
  //  handle_first_length
  //  handle_second_length
  m = [
    [ 2.9,  77.5,  23.5, 30,   26,   57, 32 ],
    [ 3.5,  102,   23.5, 30,   26,   57, 32 ],
    [ 4,    102,   23.5, 30,   26,   57, 32 ],
    [ 4.5,  102,   23.5, 30,   26,   57, 32 ],
    [ 5.5,  102,   26.7, 34.1, 29.6, 65, 36 ],
    [ 7,    125.5, 29.2, 37.4, 32,   70, 40 ]
  ];

  difference() {
    color("yellow") cube([block_width, 20, block_length]);
    translate([
      24,
      0,
      z_translation(driver_length(m[5]), block_length)]) {
      presch_screwdriver(
        m[5][0], m[5][1], m[5][2], m[5][3], m[5][4], m[5][5], m[5][6]
      );
    }

    mirror([0,0,1])
    translate([
      50,
      0,
      -z_translation(driver_length(m[4]), block_length) - driver_length(m[4])]) {
      presch_screwdriver(
        m[4][0], m[4][1], m[4][2], m[4][3], m[4][4], m[4][5], m[4][6]
      );
    }

    translate([
      75,
      0,
      z_translation(driver_length(m[3]), block_length) + 3]) {
      presch_screwdriver(
        m[3][0], m[3][1], m[3][2], m[3][3], m[3][4], m[3][5], m[3][6]
      );
    }

    mirror([0,0,1])
    translate([
      99,
      0,
      -z_translation(driver_length(m[2]), block_length) - driver_length(m[2]) + 3]) {
      presch_screwdriver(
        m[2][0], m[2][1], m[2][2], m[2][3], m[2][4], m[2][5], m[2][6]
      );
    }

    translate([
      123,
      0,
      z_translation(driver_length(m[1]), block_length) + 3]) {
      presch_screwdriver(
        m[1][0], m[1][1], m[1][2], m[1][3], m[1][4], m[1][5], m[1][6]
      );
    }

    mirror([0,0,1])
    translate([
      147,
      0,
      -z_translation(driver_length(m[0]), block_length) - driver_length(m[0]) + 13 ]) {
      presch_screwdriver(
        m[0][0], m[0][1], m[0][2], m[0][3], m[0][4], m[0][5], m[0][6]
      );
      }
      
      rotate(a=[0,90,0])
      scale([1.25 ,1,1])
      translate([-45,-1,-5]) {
        color("yellow") cylinder(
          h=block_width + 10,
          r=15,
          $fn=poly_n);
      }

      rotate(a=[0,90,0] )
      scale([1.25 ,1,1]) 
      translate([-120,-1,-5]) {
        color("yellow") cylinder(
          h=block_width + 10,
          r=15,
          $fn=poly_n);
      }
  
  }

}
