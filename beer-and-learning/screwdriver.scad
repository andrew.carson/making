poly_n = 10;

module screwdriver (
  shank_diameter,
  shank_length,
  handle_first_diameter,
  handle_second_diameter,
  handle_third_diameter,
  handle_first_length,
  handle_second_length ) {

  handle_middle_length=14;
  epsilon=0.01;
  r_increase=0.5;
  l_increase=0.75;
 
  color("grey") cylinder(
    h=shank_length+epsilon,
    r=r_increase + shank_diameter/2,
    $fn=poly_n);
    
  translate([0,0,shank_length]) {
    color("red") cylinder(
      h=l_increase+handle_first_length-(handle_middle_length/2)+epsilon,
      r1=r_increase+handle_first_diameter/2,
      r2=r_increase+handle_second_diameter/2,
      $fn=poly_n);
  }
  translate([0,0,shank_length+l_increase+handle_first_length-(handle_middle_length/2)]) {
    color("blue") cylinder(
      h=handle_middle_length,
      r1=r_increase+handle_second_diameter/2,
      r2=r_increase+handle_second_diameter/2,
      $fn=poly_n);
  }
  translate([0,0,shank_length+l_increase+handle_first_length+(handle_middle_length/2)-epsilon]) {
    color("orange") cylinder(
      h=l_increase+handle_second_length-(handle_middle_length/2)+epsilon,
      r1=r_increase+handle_second_diameter/2,
      r2=r_increase+handle_third_diameter/2,
      $fn=poly_n);
  }
}

screwdriver (3.5, 102, 23.5, 30, 26, 57, 32);